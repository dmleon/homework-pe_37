function parallax(elem, distance, speed) {
    const item = document.querySelector(elem);
    item.style.transform = `translateY(${distance * speed}px)`;
}

window.addEventListener('scroll', () => {
    parallax('.first', window.scrollY, -3);
    parallax('.second', window.scrollY, -2);
    parallax('.third', window.scrollY, -1);
});
